from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Input_Form, Registration
from .models import Kegiatan, Peserta


def tambahKegiatan(request):
	posts_kegiatan = Input_Form(request.POST or None)
	if posts_kegiatan.is_valid():
		posts_kegiatan.save()
		posts_kegiatan = Input_Form()
	context = {

		'form' : posts_kegiatan,
	}	
	return render(request,'main/create.html',context)
		
def tambahPeserta(request,id):
	posts_peserta = Registration(request.POST or None)
	if posts_peserta.is_valid():
		aktivitas = Kegiatan.objects.get(id=id)
		Peserta.objects.create(register = request.POST['register'], kegiatan=aktivitas)
		posts_peserta = Registration()
	context = {

		'form' : posts_peserta,
	}

	return render (request, 'main/form_peserta.html', context)
	
def daftarKegiatan(request):
	kegiatan = Kegiatan.objects.all()
	peserta = Peserta.objects.all()
	context = {

		'kegiatan' : kegiatan,
		'peserta' : peserta,
	}

	return render (request, 'main/daftar.html', context)





