

# Create your models here.
from django.db import models

# Create your models here.
class Kegiatan (models.Model):
    event 	= models.CharField(max_length=50,null=False,blank=False)

    def _str_(self):
        return self.event

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.CASCADE,null=False)
    register = models.CharField(max_length=50)
    
    def _str_(self):
        return self.register
  


