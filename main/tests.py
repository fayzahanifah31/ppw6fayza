from django.test import LiveServerTestCase, TestCase, tag,Client
from django.urls import reverse,resolve
from selenium import webdriver
from .views import tambahKegiatan,tambahPeserta,daftarKegiatan
from .models import Kegiatan, Peserta
from .forms import Input_Form, Registration


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:tambahKegiatan'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('main:daftarkegiatan'))
        self.assertEqual(response.status_code, 200)
    
    def test_fungsi(self):
        act = resolve('/')
        self.assertEqual(act.func, tambahKegiatan)

        listact = resolve('/daftarkegiatan/')
        self.assertEqual(listact.func, daftarKegiatan)


    def test_model_bisa_dibuat_object_baru(self):
            # Creating a new activity
            new_activity = Kegiatan.objects.create(event="Nama Kegiatan")
            new_register = Peserta.objects.create(register="Nama Peserta",kegiatan = new_activity)
        
            
            # Retrieving all available activity
            hitung_kegiatan = Kegiatan.objects.all().count()
            self.assertEqual(hitung_kegiatan, 1)
            hitung_peserta = Peserta.objects.all().count()
            self.assertEqual(hitung_peserta, 1)
    

    def test_form_validation_for_blank_items(self):
            form_form = Input_Form(data={'event':''})
            self.assertFalse(form_form.is_valid())
            self.assertEqual(
                form_form.errors['event'],
                ["This field is required."]
                
            )
            form_regist = Registration(data={'register':''})
            self.assertFalse(form_regist.is_valid())
            self.assertEqual(
                form_regist.errors['register'],
                ["This field is required."]
            )
    
    def test_lab5_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'event':test})
            self.assertEqual(response_post.status_code, 200)
    
            test2 = 'Hani'
            
            kegiatannya = Kegiatan.objects.get(event=test)
            Peserta.objects.create(register = test2, kegiatan=kegiatannya)
            response= Client().get('/daftarkegiatan/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)
            self.assertIn(test2, html_response)
    
    def test_lab5_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'event':''})
            self.assertEqual(response_post.status_code, 200)
    
            test2 = 'Hani'
            new_activity = Kegiatan.objects.create(event="Nama Kegiatan")
            Peserta.objects.create(register = '', kegiatan=new_activity)

            response= Client().get('/daftarkegiatan/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)
            self.assertNotIn(test2, html_response)
            
            



    


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
