from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.tambahKegiatan, name='tambahKegiatan'),
    path('tambahPeserta/<int:id>/', views.tambahPeserta, name='tambahPeserta'),
    path('daftarkegiatan/', views.daftarKegiatan, name='daftarkegiatan'),
    
]


