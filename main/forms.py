from django.forms import ModelForm
from django import forms
from .models import Kegiatan, Peserta
from django.db.utils import OperationalError

class Input_Form(forms.ModelForm):
	class Meta:
		model = Kegiatan
		fields = [
		'event'
		]

		labels = {
			'event' : 'Nama Kegiatan',
		}

		widgets = {
			'event' : forms.TextInput(
				attrs = {
					'class' : 'form-control',
					'placeholder' : 'Masukkan nama event',}
				)
		}

class Registration(forms.ModelForm):
	class Meta:
		model = Peserta
		fields = [
		'register',

		]

		labels = {
			'register' : 'Nama Lengkap',

		}

		widgets = {
			'register' : forms.TextInput(
				attrs = {
					'class' : 'form-control',
					'placeholder' : 'Masukkan nama Anda',}
				),


		}